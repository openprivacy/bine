module git.openprivacy.ca/openprivacy/bine

go 1.14

require (
	git.openprivacy.ca/openprivacy/log v1.0.3
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20201012173705-84dcc777aaee
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb
)
